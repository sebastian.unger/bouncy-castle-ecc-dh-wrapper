package org.ws4d.wscompactsecurity.authentication.crypto;

/*
 * This 'Wrapper' Class was supposed to become something to help
 * us develop authentication protocols that basically depend on some sort
 * of ECC Diffie Hellman. While it (somehow) accomplishes this task, I'm
 * afraid that his class became a collection of anti-patterns rather than
 * being good coding style.
 * 
 * You may, of course, use it. But you should not consider it as
 * inspiration for your own software development
 * 
 */

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Random;

import javax.crypto.KeyAgreement;

import org.bouncycastle.crypto.engines.AESFastEngine;
import org.bouncycastle.crypto.macs.CMac;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;
import org.ws4d.wscompactsecurity.authentication.crypto.types.AuthenticatedECCDHParametersType;
import org.ws4d.wscompactsecurity.authentication.crypto.types.ECC_DH_HelperType;

public class ECC_DH_Helper implements ECC_DH_HelperType {

	private AuthenticatedECCDHParametersType mParams;
	private boolean initialized;
	private boolean beHoCompliant = false;

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#isHoCompliant()
	 */
	@Override
	public boolean isHoCompliant() {
		return beHoCompliant;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#setHoCompliant(boolean)
	 */
	@Override
	public void setHoCompliant(boolean beHoCompliant) {
		this.beHoCompliant = beHoCompliant;
	}

	@Override
	public PublicKey decodePublicKey(byte[] encodedPublicKey) {
		KeyFactory fact;
		try {
			fact = KeyFactory.getInstance("ECDSA", "BC");
			return (PublicKey) fact.generatePublic(new X509EncodedKeySpec(encodedPublicKey));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public ECC_DH_Helper(AuthenticatedECCDHParametersType p) {
		/* in case you want to reuse key material */
		mParams = p;
		initialized = false;
	}

	public ECC_DH_Helper() {
		mParams = new AuthenticatedECCDHParameters();
		initialized = false;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#getParams()
	 */
	@Override
	public AuthenticatedECCDHParametersType getParams() {
		return mParams;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#setParams(org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParameters)
	 */
	@Override
	public void setParams(AuthenticatedECCDHParametersType params) {
		this.mParams = params;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#init()
	 */
	@Override
	public void init() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {
		init("secp256r1"); /* Default as spcecified in IEEE 802.15.6-2012 */
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#init(java.lang.String)
	 */
	@Override
	public void init(String curveName) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException {

		if (!initialized) {

			mParams.setCurveName(curveName);

			boolean addProvider = true;

			for (Provider p : Security.getProviders()) {
				if (p.getName().equals(BouncyCastleProvider.PROVIDER_NAME)) {
					addProvider = false;
					break;
				}
			}

			if (addProvider) {
				Security.addProvider(new BouncyCastleProvider());
			}

			if ((mParams.getCurveName() != null) && (!mParams.getCurveName().isEmpty())) {

				ECGenParameterSpec ecGenSpec = new ECGenParameterSpec(mParams.getCurveName());
				KeyPairGenerator g = KeyPairGenerator.getInstance("ECDSA", "BC");

				g.initialize(ecGenSpec, new SecureRandom());

				if (mParams.getKeyPair() == null) { /* don't overwrite in case there already is any */
					mParams.setKeyPair(g.generateKeyPair());
				}
				mParams.addOtherA(curveName);
				mParams.addOtherB(curveName);
				initialized = true;
			} else {
				initialized = false;
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#setForeignPublicKey(java.security.PublicKey)
	 */
	@Override
	public void setForeignPublicKey(PublicKey k) {
		mParams.setForeignPublicKey(k);
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#calculateSharedSecret()
	 */
	@Override
	public void calculateSharedSecret() throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
		calculateSharedSecret(mParams.getForeignPublicKey());
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#calculateSharedSecret(java.security.PublicKey)
	 */
	@Override
	public void calculateSharedSecret(PublicKey priv) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
		KeyAgreement ka = KeyAgreement.getInstance("ECDH", "BC");
		ka.init(mParams.getKeyPair().getPrivate());
		ka.doPhase(mParams.getForeignPublicKey(), true);
		mParams.setSessionKey(ka.generateSecret());
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#OOBkey2Integer()
	 */
	@Override
	public void OOBkey2Integer() {
		/* Leftmost bits become MSBs */
		int result = 0;
		for (int i = 0; i < mParams.getOOBSharedSecret().length; i++) {
			result += ((mParams.getOOBSharedSecret()[i]) ? 1 : 0) * (1 << (mParams.getOOBSharedSecret().length-1-i));
		}
		mParams.setOOBSharedSecretAsInt(result);
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#OOBkeyFromInteger()
	 */
	@Override
	public void OOBkeyFromInteger() {
		boolean[] key = new boolean[20]; //FIXME -,-'
		int ikey = mParams.getOOBSharedSecretAsInt();
		for (int i=0; i < key.length; i++) {
			key[i] = (ikey & (1 << (20-i-1))) > 0;
		}
		mParams.setOOBSharedSecret(key);
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#encryptPublicKey()
	 */
	@Override
	public void encryptPublicKey() {
		if (beHoCompliant) {
			/* TODO: Real Encryption */
			mParams.setDistortedPublicKey(mParams.getKeyPair().getPublic());
		} else {
			mParams.setDistortedPublicKey(mParams.getKeyPair().getPublic());
		}
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#decryptPublicKey()
	 */
	@Override
	public void decryptPublicKey() {
		if (beHoCompliant) {
			/* TODO: Alike: real decryption */
			mParams.setForeignPublicKey(mParams.getDistortedPublicKey());
		} else {
			mParams.setForeignPublicKey(mParams.getDistortedPublicKey());
		}
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#calculateCMAC()
	 */
	@Override
	public void calculateCMAC() {
		CMac cmac = new CMac(new AESFastEngine(), 64);

		KeyParameter key = null;

		if (beHoCompliant) {
			byte[] dhKeyPart = new byte[16];
			for (int i=0; i < 16; i++) {
				dhKeyPart[i] = mParams.getSessionKey()[16+i]; /* Right-most 128 bits */
			}

			key = new KeyParameter(dhKeyPart);
		} else {
			byte[] keyArray = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; /* ouch... */
			keyArray[15] = (byte) (mParams.getOOBSharedSecretAsInt() & 0xFF);
			keyArray[14] = (byte) ((mParams.getOOBSharedSecretAsInt() >> 8)  & 0xFF);
			keyArray[13] = (byte) ((mParams.getOOBSharedSecretAsInt() >> 16) & 0xFF);
			key = new KeyParameter(keyArray);
		}

		System.out.print("Key:");
		for (int i = 0; i < key.getKey().length; i++) {
			System.out.printf(" %02x", key.getKey()[i]);
		}
		System.out.print("\n");

		cmac.init(key);

		cmac.update(mParams.getOrigin().getBytes(), 0, mParams.getOrigin().getBytes().length);
		System.out.println("calculateCMAC: Origin: " + mParams.getOrigin());
		cmac.update(mParams.getTarget().getBytes(), 0, mParams.getTarget().getBytes().length);
		System.out.println("calculateCMAC: Target: " + mParams.getTarget());

		cmac.update(mParams.getNonceA(), 0, mParams.getNonceA().length);
		System.out.println("calculateCMAC: Origin Nonce: " + Base64.toBase64String(mParams.getNonceA()));
		cmac.update(mParams.getNonceB(), 0, mParams.getNonceB().length);
		System.out.println("calculateCMAC: Target Nonce: " + Base64.toBase64String(mParams.getNonceB()));

		String otherA = mParams.otherAasString();
		System.out.println("calculateCMAC: Other: " + otherA);

		cmac.update(otherA.getBytes(), 0, otherA.getBytes().length);

		byte[] out = new byte[8];

		cmac.doFinal(out, 0);

		mParams.setCMAC(out);

	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#checkCMAC()
	 */
	@Override
	public boolean checkCMAC() {

		CMac cmac = new CMac(new AESFastEngine(), 64);

		KeyParameter key = null;

		if (beHoCompliant) {
			byte[] dhKeyPart = new byte[16];
			for (int i=0; i < 16; i++) {
				dhKeyPart[i] = mParams.getSessionKey()[16+i]; /* Right-most 128 bits */
			}

			key = new KeyParameter(dhKeyPart);
		} else {
			byte[] keyArray = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; /* ouch... */
			keyArray[15] = (byte) (mParams.getOOBSharedSecretAsInt() & 0xFF);
			keyArray[14] = (byte) ((mParams.getOOBSharedSecretAsInt() >> 8)  & 0xFF);
			keyArray[13] = (byte) ((mParams.getOOBSharedSecretAsInt() >> 16) & 0xFF);
			key = new KeyParameter(keyArray);
		}

		System.out.print("Key:");
		for (int i = 0; i < key.getKey().length; i++) {
			System.out.printf(" %02x", key.getKey()[i]);
		}
		System.out.print("\n");

		cmac.init(key);

		cmac.update(mParams.getTarget().getBytes(), 0, mParams.getTarget().getBytes().length);
		System.out.println("CheckCMAC: Origin: " + mParams.getTarget());
		cmac.update(mParams.getOrigin().getBytes(), 0, mParams.getOrigin().getBytes().length);
		System.out.println("CheckCMAC: Target: " + mParams.getOrigin());

		cmac.update(mParams.getNonceB(), 0, mParams.getNonceB().length);
		System.out.println("CheckCMAC: Origin Nonce: " + Base64.toBase64String(mParams.getNonceB()));
		cmac.update(mParams.getNonceA(), 0, mParams.getNonceA().length);
		System.out.println("CheckCMAC: Target Nonce: " + Base64.toBase64String(mParams.getNonceA()));

		String otherB = mParams.otherBasString();

		cmac.update(otherB.getBytes(), 0, otherB.getBytes().length);

		System.out.println("CheckCMAC: Other: " + otherB);

		byte[] out = new byte[8];

		cmac.doFinal(out, 0);

		for (int i = 0; i < mParams.getForeignCMAC().length; i++) {
			if (mParams.getForeignCMAC()[i] != out[i])
				return false;
		}

		return true;

	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#calculateMasterKey()
	 */
	@Override
	public void calculateMasterKey() {
		CMac cmac = new CMac(new AESFastEngine(), 128);

		byte[] dhKeyPart = new byte [16];

		for (int i=0 ; i < 16; i++) {
			dhKeyPart[i] = mParams.getSessionKey()[i]; /* Left-most 128 bits */
		}

		KeyParameter key = new KeyParameter(dhKeyPart);

		cmac.init(key);

		byte[] material = new byte[16]; /* 128 bit Nonces XORed */

		byte[] nA = mParams.getNonceA();
		byte[] nB = mParams.getNonceB();

		for (int i = 0; i < 16; i++) {
			material[i] = (byte) (nA[i] ^ nB[i]);
		}

		cmac.update(material, 0, material.length);

		byte[] out = new byte[16];

		cmac.doFinal(out, 0);

		mParams.setMasterKey(out);

	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_HelperType#createNonce()
	 */
	@Override
	public void createNonce() {
		byte[] nonce = new byte[16];
		Random r = new Random();
		r.nextBytes(nonce);
		mParams.setNonceA(nonce);
		/* TODO: Maybe, cryptographically secure random numbers might be useful */
	}

	@Override
	public int generateSymmetricKey(byte[] keyBuf) {
		// FIXME!!
		// SRSLY!!
		// Do this cryptographically secure!!
		Random r = new Random(System.currentTimeMillis());
		r.nextBytes(keyBuf);
		return keyBuf.length;
	}

}
