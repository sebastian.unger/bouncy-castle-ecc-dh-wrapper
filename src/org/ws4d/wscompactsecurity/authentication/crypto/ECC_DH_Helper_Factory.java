package org.ws4d.wscompactsecurity.authentication.crypto;

import java.util.Locale;

import org.ws4d.wscompactsecurity.authentication.crypto.types.ECC_DH_HelperType;


public class ECC_DH_Helper_Factory {
	public static ECC_DH_HelperType getHelper() {
		String runtime = System.getProperty("java.runtime.name", "WAT VM");
		ECC_DH_HelperType result = null;
		if (runtime.toLowerCase(Locale.ENGLISH).contains("android")) {
			try {
				result = (ECC_DH_HelperType) Class.forName("org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_Helper_Android").newInstance();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (result == null)
					System.err.println("Android detected but Class could not be found");
			}
		} else {
			try {
				result = (ECC_DH_HelperType) Class.forName("org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_Helper").newInstance();
			} catch (ClassNotFoundException e) {
				System.err.println("Going for default no work! Going hard way now!");
				result = new ECC_DH_Helper();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (result == null) {
			System.err.println("VERY last resort. Going for default AGAIN!");
			result = new ECC_DH_Helper();
		}
		return result;
	}
}
