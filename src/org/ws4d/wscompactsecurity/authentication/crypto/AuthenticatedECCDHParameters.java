package org.ws4d.wscompactsecurity.authentication.crypto;

import java.security.KeyPair;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import org.ws4d.wscompactsecurity.authentication.crypto.types.AuthenticatedECCDHParametersType;


public class AuthenticatedECCDHParameters implements AuthenticatedECCDHParametersType {
	
	String mOrigin = null;
	String mTarget = null;
	
	byte[] mNonceA;
	byte[] mNonceB;
	
	String mCurveName = null;
	KeyPair mKeyPair = null;
	
	PublicKey mForeignPublicKey = null;
	PublicKey mDistortedPublicKey = null;
	
	boolean[] mOOBSharedSecret = null;
	int mOOBSharedSecretAsInt;
	byte[] mSessionKey = null;
	byte[] mMasterKey = null;
	byte[] mCMAC = null;
	byte[] mForeignCMAC = null;
	
	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getForeignCMAC()
	 */
	@Override
	public byte[] getForeignCMAC() {
		return mForeignCMAC;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setForeignCMAC(byte[])
	 */
	@Override
	public void setForeignCMAC(byte[] foreignCMAC) {
		this.mForeignCMAC = foreignCMAC;
	}

	HashSet<String> mOtherA = null;
	HashSet<String> mOtherB = null;
	
	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getOtherA()
	 */
	@Override
	public HashSet<String> getOtherA() {
		return mOtherA;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setOtherA(java.util.HashSet)
	 */
	@Override
	public void setOtherA(HashSet<String>  otherA) {
		this.mOtherA = otherA;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getOtherB()
	 */
	@Override
	public HashSet<String> getOtherB() {
		return mOtherB;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setOtherB(java.util.HashSet)
	 */
	@Override
	public void setOtherB(HashSet<String> otherB) {
		this.mOtherB = otherB;
	}
	
	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#addOtherA(java.lang.String)
	 */
	@Override
	public void addOtherA(String s) {
		this.mOtherA.add(s);
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#addOtherB(java.lang.String)
	 */
	@Override
	public void addOtherB(String s) {
		this.mOtherB.add(s);
	}
	
	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#otherAasString()
	 */
	@Override
	public String otherAasString() {
		String result = "";
		
		/* HashSets do not allow duplicates (good) but don't allow sorting either (bad)
		 * However, ArrayList allows sorting (good) but also allows duplicated (bad) */
		ArrayList<String> sorted = new ArrayList<String>();
		
		sorted.addAll(mOtherA);
		
		Collections.sort(sorted);
		
		for (String s : sorted) {
			result += s;
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#otherBasString()
	 */
	@Override
	public String otherBasString() {
		String result = "";
		
		ArrayList<String> sorted = new ArrayList<String>();
		
		sorted.addAll(mOtherB);
		
		Collections.sort(sorted);
		
		for (String s : sorted) {
			result += s;
		}
		
		return result;
	}

	boolean mIsFirst = false; /* whether or not this participant is the first one
							  * to transmit its public key, which means it gets
							  * scrambled
							  * ************************************************ */
	
	public AuthenticatedECCDHParameters() {
		mOtherA = new HashSet<String>();
		mOtherB = new HashSet<String>();
	}
	
	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getIsFirst()
	 */
	@Override
	public boolean getIsFirst() {
		return mIsFirst;
	}
	
	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setIsFirst(boolean)
	 */
	@Override
	public void setIsFirst(boolean isFirst) {
		this.mIsFirst = isFirst;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getOrigin()
	 */
	@Override
	public String getOrigin() {
		return mOrigin;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setOrigin(java.lang.String)
	 */
	@Override
	public void setOrigin(String origin) {
		this.mOrigin = origin;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getTarget()
	 */
	@Override
	public String getTarget() {
		return mTarget;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setTarget(java.lang.String)
	 */
	@Override
	public void setTarget(String target) {
		this.mTarget = target;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getNonceA()
	 */
	@Override
	public byte[] getNonceA() {
		return mNonceA;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setNonceA(byte[])
	 */
	@Override
	public void setNonceA(byte[] nonce) {
		this.mNonceA = nonce;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getNonceB()
	 */
	@Override
	public byte[] getNonceB() {
		return mNonceB;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setNonceB(byte[])
	 */
	@Override
	public void setNonceB(byte[] nonce) {
		this.mNonceB = nonce;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getCurveName()
	 */
	@Override
	public String getCurveName() {
		return mCurveName;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setCurveName(java.lang.String)
	 */
	@Override
	public void setCurveName(String curveName) {
		this.mCurveName = curveName;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getKeyPair()
	 */
	@Override
	public KeyPair getKeyPair() {
		return mKeyPair;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setKeyPair(java.security.KeyPair)
	 */
	@Override
	public void setKeyPair(KeyPair keyPair) {
		this.mKeyPair = keyPair;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getForeignPublicKey()
	 */
	@Override
	public PublicKey getForeignPublicKey() {
		return mForeignPublicKey;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setForeignPublicKey(java.security.PublicKey)
	 */
	@Override
	public void setForeignPublicKey(PublicKey foreignPublicKey) {
		this.mForeignPublicKey = foreignPublicKey;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getDistortedPublicKey()
	 */
	@Override
	public PublicKey getDistortedPublicKey() {
		return mDistortedPublicKey;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setDistortedPublicKey(java.security.PublicKey)
	 */
	@Override
	public void setDistortedPublicKey(PublicKey distortedPublicKey) {
		this.mDistortedPublicKey = distortedPublicKey;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getCMAC()
	 */
	@Override
	public byte[] getCMAC() {
		return mCMAC;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setCMAC(byte[])
	 */
	@Override
	public void setCMAC(byte[] cMAC) {
		mCMAC = cMAC;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getOOBSharedSecret()
	 */
	@Override
	public boolean[] getOOBSharedSecret() {
		return mOOBSharedSecret;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setOOBSharedSecret(boolean[])
	 */
	@Override
	public void setOOBSharedSecret(boolean[] oobSharedSecret) {
		this.mOOBSharedSecret = oobSharedSecret;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getOOBSharedSecretAsInt()
	 */
	@Override
	public int getOOBSharedSecretAsInt() {
		return mOOBSharedSecretAsInt;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setOOBSharedSecretAsInt(int)
	 */
	@Override
	public void setOOBSharedSecretAsInt(int oobSharedSecret) {
		this.mOOBSharedSecretAsInt = oobSharedSecret;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getSessionKey()
	 */
	@Override
	public byte[] getSessionKey() {
		return mSessionKey;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setSessionKey(byte[])
	 */
	@Override
	public void setSessionKey(byte[] sessionKey) {
		this.mSessionKey = sessionKey;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#getMasterKey()
	 */
	@Override
	public byte[] getMasterKey() {
		return mMasterKey;
	}

	/* (non-Javadoc)
	 * @see org.ws4d.wscompactsecurity.authentication.crypto.AuthenticatedECCDHParametersType#setMasterKey(byte[])
	 */
	@Override
	public void setMasterKey(byte[] masterKey) {
		this.mMasterKey = masterKey;
	}

	
	
}
