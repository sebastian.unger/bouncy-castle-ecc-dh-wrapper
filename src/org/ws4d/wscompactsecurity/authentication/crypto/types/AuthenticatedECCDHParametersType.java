package org.ws4d.wscompactsecurity.authentication.crypto.types;

import java.security.KeyPair;
import java.security.PublicKey;
import java.util.HashSet;

public interface AuthenticatedECCDHParametersType {

	public abstract byte[] getForeignCMAC();

	public abstract void setForeignCMAC(byte[] foreignCMAC);

	public abstract HashSet<String> getOtherA();

	public abstract void setOtherA(HashSet<String> otherA);

	public abstract HashSet<String> getOtherB();

	public abstract void setOtherB(HashSet<String> otherB);

	public abstract void addOtherA(String s);

	public abstract void addOtherB(String s);

	public abstract String otherAasString();

	public abstract String otherBasString();

	public abstract boolean getIsFirst();

	public abstract void setIsFirst(boolean isFirst);

	public abstract String getOrigin();

	public abstract void setOrigin(String origin);

	public abstract String getTarget();

	public abstract void setTarget(String target);

	public abstract byte[] getNonceA();

	public abstract void setNonceA(byte[] nonce);

	public abstract byte[] getNonceB();

	public abstract void setNonceB(byte[] nonce);

	public abstract String getCurveName();

	public abstract void setCurveName(String curveName);

	public abstract KeyPair getKeyPair();

	public abstract void setKeyPair(KeyPair keyPair);

	public abstract PublicKey getForeignPublicKey();

	public abstract void setForeignPublicKey(PublicKey foreignPublicKey);

	public abstract PublicKey getDistortedPublicKey();

	public abstract void setDistortedPublicKey(PublicKey distortedPublicKey);

	public abstract byte[] getCMAC();

	public abstract void setCMAC(byte[] cMAC);

	public abstract boolean[] getOOBSharedSecret();

	public abstract void setOOBSharedSecret(boolean[] oobSharedSecret);

	public abstract int getOOBSharedSecretAsInt();

	public abstract void setOOBSharedSecretAsInt(int oobSharedSecret);

	public abstract byte[] getSessionKey();

	public abstract void setSessionKey(byte[] sessionKey);

	public abstract byte[] getMasterKey();

	public abstract void setMasterKey(byte[] masterKey);

}