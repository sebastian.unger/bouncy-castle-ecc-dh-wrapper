package org.ws4d.wscompactsecurity.authentication.crypto.types;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;


public interface ECC_DH_HelperType {

	public abstract boolean isHoCompliant();

	public abstract void setHoCompliant(boolean beHoCompliant);

	public abstract AuthenticatedECCDHParametersType getParams();

	public abstract void setParams(AuthenticatedECCDHParametersType params);

	public abstract void init() throws NoSuchAlgorithmException,
	NoSuchProviderException, InvalidAlgorithmParameterException;

	public abstract void init(String curveName)
			throws NoSuchAlgorithmException, NoSuchProviderException,
			InvalidAlgorithmParameterException;

	public abstract void setForeignPublicKey(PublicKey k);

	public abstract void calculateSharedSecret()
			throws NoSuchAlgorithmException, NoSuchProviderException,
			InvalidKeyException;

	public abstract void calculateSharedSecret(PublicKey priv)
			throws NoSuchAlgorithmException, NoSuchProviderException,
			InvalidKeyException;

	public abstract void OOBkey2Integer();

	public abstract void OOBkeyFromInteger();

	public abstract void encryptPublicKey();

	public abstract void decryptPublicKey();

	public abstract void calculateCMAC();

	public abstract boolean checkCMAC();

	public abstract void calculateMasterKey();

	public abstract void createNonce();

	public abstract PublicKey decodePublicKey(byte[] encodedPublicKey);

	public abstract int generateSymmetricKey(byte[] keyBuf);

}