package org.ws4d.wscompactsecurity.authentication.crypto.test;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.interfaces.ECPublicKey;

import org.ws4d.wscompactsecurity.authentication.crypto.ECC_DH_Helper;
import org.ws4d.wscompactsecurity.authentication.crypto.types.ECC_DH_HelperType;


public class ECC_DiffieHellman_TEST {


	public static void main(String[] args) {

		ECC_DH_HelperType alice = new ECC_DH_Helper();
		ECC_DH_HelperType bob = new ECC_DH_Helper();

		try {
			alice.init(); /* generates EC DH public and private key */
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return;
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
			return;
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
			return;
		}

		alice.getParams().setOrigin("alice");
		alice.getParams().setTarget("bob");

		alice.getParams().addOtherA("ws4d:authentication-mechanism1");
		/* otherA and otherB are collections of 'additional parameters' of (A)lice
		 * and (B)ob that are incorporated into the CMAC. To date, these 'additional
		 * parameters' are the used curve name (set implicitly by the wrapper class)
		 * and the used authentication mechanism
		 */

		/*
		 * 1st handshake message: from alice to bob
		 * ------------------------------------------------------------------------------->
		 * 
		 *   - origin (e.g. Alice or Alice's UUID)
		 *   - target (e.g. Bob or Bob's UUID)
		 *   - The mechanism to authenticate with
		 *   - optional: a curve name (esp. if it differs from default value "secp256r1")
		 * 
		 */
		
																		try {
																			bob.init(alice.getParams().getCurveName());
																		} catch (NoSuchAlgorithmException e) {
																			e.printStackTrace();
																			return;
																		} catch (NoSuchProviderException e) {
																			e.printStackTrace();
																			return;
																		} catch (InvalidAlgorithmParameterException e) {
																			e.printStackTrace();
																			return;
																		}

																		bob.getParams().setOrigin("bob");
																		bob.getParams().setTarget("alice");
																		
																		bob.getParams().addOtherB("ws4d:authentication-mechanism1");
																		bob.getParams().addOtherA("ws4d:authentication-mechanism1");
																		
																		/* Bob created OOB Shared secret */
																		boolean[] oobSharedSecret = {true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true, false};
																		
																		bob.getParams().setOOBSharedSecret(oobSharedSecret);
																		bob.OOBkey2Integer(); /* calculates integer from binary form */
																		
																		System.out.print("Bob's OOB shared secret:");
																		for (int i=0; i<oobSharedSecret.length; i++) {
																			System.out.print(oobSharedSecret[i] ? "1" : "0");
																		}
																		int intOOBss = bob.getParams().getOOBSharedSecretAsInt();
																		System.out.println(" (" + intOOBss + ")");
																		
																		bob.createNonce();
																		bob.encryptPublicKey(); /* the transmitted public Key P_b' = f(P_b, OOBsharedSecret) */
																		
																		byte[] bobsEncodedPublicKey = bob.getParams().getDistortedPublicKey().getEncoded();
																		System.out.println(bob.getParams().getDistortedPublicKey().getAlgorithm() + " " + ((ECPublicKey)bob.getParams().getDistortedPublicKey()).getFormat());
																		
		/* 
		 * 2nd handshake message: from bob to alice
		 * <---------------------------------------------------------------------------------
		 * 
		 *   - origin: bob
		 *   - target: alice
		 *   - mechanism
		 *   - Bob's Nonce
		 *   - Bob's encrypted public key
		 * 
		 */

		alice.getParams().setNonceB(bob.getParams().getNonceA()); /* of course, this and other data
		                                                           * is taken from the message */
		alice.getParams().addOtherB("ws4d:authentication-mechanism1");


		alice.getParams().setDistortedPublicKey(alice.decodePublicKey(bobsEncodedPublicKey));

		alice.createNonce();
		
		
		                /* 
		                 * Magic happens: An OOB shared secret is exchanged
		                 ********************************************************** */
		
		alice.getParams().setOOBSharedSecretAsInt(intOOBss);
		alice.OOBkeyFromInteger();

		System.out.print("Alice's OOB shared secret:");
		for (int i=0; i<alice.getParams().getOOBSharedSecret().length; i++) {
			System.out.print(alice.getParams().getOOBSharedSecret()[i] ? "1" : "0");
		}
		System.out.println(" (" + alice.getParams().getOOBSharedSecretAsInt() + ")");

		alice.decryptPublicKey(); /* The original public key P_b = f(P_b', OOBSharedSecret) can be computed */

		try {
			alice.calculateSharedSecret();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return;
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
			return;
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		alice.calculateCMAC();

		System.out.print("Alice's Session Key:");
		for (int i = 0 ; i < alice.getParams().getSessionKey().length; i++) {
			System.out.printf(" %02x", alice.getParams().getSessionKey()[i]);
		}
		System.out.println(" (" + alice.getParams().getSessionKey().length + " byte)");

		System.out.print("Alice's calculated MAC:");
		for (int i = 0; i < alice.getParams().getCMAC().length; i++) {
			System.out.printf(" %02x", alice.getParams().getCMAC()[i]);
		}
		System.out.printf("\n");

		byte[] alicesEncodedPublicKey = alice.getParams().getKeyPair().getPublic().getEncoded();

		/*
		 * 3rd handshake message: from alice to bob
		 * ------------------------------------------------------------------------------->
		 * 
		 *   - origin (e.g. Alice or Alice's UUID)
		 *   - target (e.g. Bob or Bob's UUID)
		 *   - Alice's Nonce
		 *   - Alice's public key
		 *   - Alice's CMAC
		 * 
		 */
		
																		bob.setForeignPublicKey(bob.decodePublicKey(alicesEncodedPublicKey)); /* same here: of course, this comes from the message */
																		bob.getParams().setNonceB(alice.getParams().getNonceA());
																		bob.getParams().setForeignCMAC(alice.getParams().getCMAC());
																		
																		try {
																			bob.calculateSharedSecret();
																		} catch (NoSuchAlgorithmException e) {
																			e.printStackTrace();
																			return;
																		} catch (NoSuchProviderException e) {
																			e.printStackTrace();
																			return;
																		} catch (InvalidKeyException e) {
																			// TODO Auto-generated catch block
																			e.printStackTrace();
																		}
																		bob.calculateCMAC();
																		
																		System.out.print("Bob's Session Key:");
																		for (int i = 0 ; i < bob.getParams().getSessionKey().length; i++) {
																			System.out.printf(" %02x", bob.getParams().getSessionKey()[i]);
																		}
																		System.out.println(" (" + bob.getParams().getSessionKey().length + " byte)");
																		
																		System.out.print("Bob's calculated MAC:");
																		for (int i = 0; i < bob.getParams().getCMAC().length; i++) {
																			System.out.printf(" %02x", bob.getParams().getCMAC()[i]);
																		}
																		System.out.printf("\n");
																		
																		System.out.println("Did Alice's CMAC pass test? " + bob.checkCMAC());
																		
		/* 
		 * 4th handshake message: from bob to alice
		 * <---------------------------------------------------------------------------------
		 * 
		 *   - origin: bob
		 *   - target: alice
		 *   - Bob's CMAC
		 * 
		 */

		alice.getParams().setForeignCMAC(bob.getParams().getCMAC());

		System.out.println("Did Bob's CMAC pass test? " + alice.checkCMAC());

		/*
		 * 
		 *               DONE!!          Alice and Bob can now calculate the master key!
		 * 
		 */

		alice.calculateMasterKey();
																		bob.calculateMasterKey();
																		
																		
																		
		
		System.out.print("Alice's MASTER Key:");
		for (int i = 0 ; i < alice.getParams().getMasterKey().length; i++) {
			System.out.printf(" %02x", alice.getParams().getMasterKey()[i]);
		}
		System.out.println(" (" + alice.getParams().getMasterKey().length + " byte)");
		
		
		
		
																		System.out.print("Bob's   MASTER Key:");
																		for (int i = 0 ; i < bob.getParams().getMasterKey().length; i++) {
																			System.out.printf(" %02x", bob.getParams().getMasterKey()[i]);
																		}
																		System.out.println(" (" + bob.getParams().getMasterKey().length + " byte)");
																		
																		
	}


}
